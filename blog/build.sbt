name := """blog"""
organization := "net.webbureaucrat"

version := "1.0-SNAPSHOT"

lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .enablePlugins(JavaAppPackaging)

scalaVersion := "2.12.3"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test
libraryDependencies += "org.mongodb.scala" %% "mongo-scala-driver" % "2.4.1"
libraryDependencies += "io.netty" % "netty-all" % "4.1.29.Final"

scalacOptions += "-feature"
scalacOptions += "-target:jvm-1.8"
//enablePlugins(DockerPlugin)
packageName in Docker := "webbureaucrat-container"
dockerExposedPorts := Seq(9000)
dockerRepository := Some("eholley")
// Adds additional packages into Twirl
//TwirlKeys.templateImports += "net.webbureaucrat.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "net.webbureaucrat.binders._"
