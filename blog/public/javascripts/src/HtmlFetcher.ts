export class HtmlFetcher
{
    public getPreviewTemplate():Promise<string>
	{
	return fetch("/preview").then(response => response.text());
    }
}
