import { Article } from "./Article.js";
import { DataFetcher } from "./DataFetcher.js";

document.addEventListener('DOMContentLoaded', () => new PageEvents().onLoad())

class PageData
{
    articleElement: HTMLElement;
    title: HTMLHeadingElement;
    description: HTMLParagraphElement;
    timestamp: HTMLTimeElement;
    btn: HTMLInputElement;

    target:HTMLElement;

    private frag: DocumentFragment = <DocumentFragment>(<HTMLTemplateElement>document.getElementById('template-preview')).content.cloneNode(true);
    
    constructor()
    {
        console.log('initializing PageData().');
        this.articleElement = <HTMLElement> this.frag.querySelector('article');
        this.title = <HTMLHeadingElement>this.frag.querySelector('.h2-title');
        this.description = <HTMLParagraphElement>this.frag.querySelector('.description');
        this.timestamp = <HTMLTimeElement>this.frag.querySelector('.time-submission');
        this.btn = <HTMLInputElement>this.frag.querySelector('.btn-to-article');
        this.target = <HTMLElement>document.querySelector('#preview-target');
        console.log('initialized PageData.');
    }

    bind(article: Article): void
    {
        console.log('binding article...');
        this.title.textContent = article.title;
        this.description.textContent = article.description
        this.timestamp.textContent = article.created.toString();
        this.target.appendChild(this.articleElement);
        console.log('article bound.');
    }
    
}

class PageEvents
{
    onLoad(): void
    {
        console.log('page loaded.');
        new DataFetcher().getRecent()
            .then(recents => this.bindAll(recents));
    }

    bindAll(articles: Array<Article>): void
    {
        console.log('got articles.');
        console.log('Number of articles to bind: ' + articles.length);
        articles.map(article => new PageData().bind(article));
    }

    bind(article: Article): void
    {
        /*/
        let templatePreview = <HTMLTemplateElement>document.getElementById("template-preview")
            || new HTMLTemplateElement();
        let frag = <DocumentFragment>(templatePreview.content
                    || new DocumentFragment()).cloneNode(true);
        let articleTemplate = frag.firstElementChild || new HTMLElement();
        let title = articleTemplate.firstChild
            || new HTMLHeadingElement();
        let description = title.nextSibling
            || new HTMLParagraphElement();
        let timestamp = description.nextSibling
            || new HTMLTimeElement();
        let btn = timestamp.nextSibling || new HTMLAnchorElement();

        title.textContent = article.title;
        description.textContent = article.description;
        timestamp.textContent = article.created.toString(); //TODO: consider "updated"?
        //TODO: btn
        
        const pd = new PageData();
        pd.title.textContent = article.title;
        pd.description.textContent = article.description
        pd.timestamp.textContent = article.created.toString();
        let target = document.getElementById('preview-target')
            || new HTMLElement;
        target.appendChild(articleTemplate);
        //*/
    }
}

    
