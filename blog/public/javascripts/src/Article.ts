import { ArticleDto } from "./ArticleDto";

export class Article
{
    _id: string
    content: string
    description: string
    title: string
    url: string
    created: Date
    updated: Date

    constructor(obj: ArticleDto)
    {
	this._id = obj._id;
	this.content = obj.content;
	this.description = obj.content;
	this.title = obj.title;
	this.url = obj.url;
	this.created = new Date(obj.created);
	this.updated = new Date(obj.updated);
    }
}
