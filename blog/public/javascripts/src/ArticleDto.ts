export class ArticleDto
{
    _id: string;
    content: string;
    description: string;
    title: string;
    url: string;
    created: string;
    updated: string;
    
    constructor
    (
        _id: string, content: string, description: string, title: string,
        url: string, created: string, updated: string
    )
    {
        this._id = _id;
        this.content = content;
        this.description = description;
        this.title = title;
        this.url = url;
        this.created = created;
        this.updated = updated;
    }
}

