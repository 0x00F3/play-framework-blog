function main():void
{
    const initialize:Promise<any> = fetch("/preview")
    	  .then(response => response.text())
	  .then(text => alert(text));
}

document.addEventListener("DOMContentLoaded", main);
