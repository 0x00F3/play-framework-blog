export class ArticleMetaData
{
    action:string = "";
    description:string = "";
    id:number = -1;
    posted:string = "";
    title:string = "";
}
