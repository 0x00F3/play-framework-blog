import { ArticleMetaData } from "./ArticleMetaData.js";
import { Article } from "./Article.js";
import { ArticleDto } from "./ArticleDto.js";

export class DataFetcher
{
    public getArticleMetaData(k:number):Promise<ArticleMetaData>
	{
	return fetch("/metadata/" + k)
	    .then(response => response.text())
	    .then(json => <ArticleMetaData>JSON.parse(json));
    }

    
    public getMostRecentMetadata():Promise<string>
	{
	return fetch("mostRecentRecentMetadata")
	    .then(response => response.text());
    }

    /*new code 2018-12-01*/
    public getRecent(): Promise<Array<Article>> 
	{
        console.log('getting articles...');
	return fetch("/recent")
	    .then(response => response.text())
	    .then(json => <Array<ArticleDto>>JSON.parse(json))
	    .then(dtos => dtos.map(dto => new Article(dto)));
	}
}
