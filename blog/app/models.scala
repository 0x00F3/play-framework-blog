package models;

/* case class view models */
import domain.DTO._
import java.text.SimpleDateFormat
import java.util._
import org.mongodb.scala.bson._
import scala.language.postfixOps

case class DateModel(underlying: Date)
{
  override def toString() =
  {
    val sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
    sdf setTimeZone TimeZone.getTimeZone("UTC");
    sdf format underlying
  }
}
    

/* the Entry is the view model for an Article */
case class Entry
(
  title: String,
  content: String,
  description: String,
  url: String
)
{
  def toArticle: Article =
  {
    Article(
      new ObjectId,
      content, //html
      description, //html
      title,
      url,
      new Date,
      new Date,
      true
    )
  } 
};

case class LoginModel(username: String, password: String);
