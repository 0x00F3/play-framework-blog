package controllers;

import javax.inject._
import play.api._
import play.api.data._
import play.api.data.Forms._
import play.api.mvc._
import models._
@Singleton
class LoginController @Inject()(cc: ControllerComponents, config: Configuration)
    extends AbstractController(cc)
{
  val loginForm = Form(
    mapping(
      "username" -> text,
      "password" -> text
    )(LoginModel.apply)(LoginModel.unapply)
  )

  def index = Action
  {
    implicit request:Request[AnyContent] => Ok(views.html.login())
  }

  def submit = Action
  {
    implicit request:Request[AnyContent] =>
    {
      loginForm.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.login()),
        userData =>
        {
          if (userData == realUser)
          {
            Redirect("/write")
              .withSession("username" -> userData.username)
          }
          else
          {
            Unauthorized("<h1>Authentication failure, sorry</h1>")
          }
        }
      )
    }
  }

  /*--------------------------helper methods--------------------------------*/
  def realUser:LoginModel =
  {
    LoginModel("admin", config.get[String]("adminPassword"))
  }
}
