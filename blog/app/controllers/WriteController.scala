package controllers;

/* controller for writing a new article */
import domain._
import javax.inject._
import models._
import org.mongodb.scala._
import play.api._
import play.api.data._
import play.api.data.Forms._
import play.api.mvc._
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.language.postfixOps
import scala.util._
import websupport._

@Singleton
class WriteController @Inject()(
  cc: ControllerComponents,
  config: Configuration,
  authenticated: AuthenticatedAction
)
    extends AbstractController(cc)
{
  val writeForm = Form(
    mapping(
      "title" -> text,
      "content" -> text,
      "description" -> text,
      "url" -> text,
    )(Entry.apply)(Entry.unapply)
  )

  def index = authenticated
  {
    implicit request: Request[AnyContent] => Ok(views.html.write())
  }

  def submit = authenticated.async
  {
    implicit request: Request[AnyContent] =>
    {
      writeForm.bindFromRequest.fold(
        formWithErrors => Future { BadRequest(views.html.write()) },
        entry => new ArticleRepository(config get[String] "connectionString")
          .create(entry toArticle)
          .map(completed => Ok("Thanks for the article"))
      )
    }
  }

  /*------------------------helpers------------------------------------*/
  def saveEntry(entry: Entry): Future[Result] = 
  {
    new ArticleRepository(config get[String] "connectionString")
      .create(entry toArticle)
      .flatMap {
        completed => Future.successful(Ok("Thanks for the article" + completed.toString))
      }
  }
/*
  def thank(f: Future[Completed]): Future[Result] =
  {
    f match
    {
      case Success(completed) => Ok("Thanks for the article")
      case Failure(completed) => Ok("Oops")
    }
  }
 */
}
