package controllers;
import domain._
import domain.DTO._
import java.time._
import javax.inject._
import play.api._
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.language.postfixOps
/*
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(cc: ControllerComponents, config:Configuration)
    extends AbstractController(cc)
{

  implicit val tocWrites = new Writes[ArticleMetaData]
  {
    def writes(article:ArticleMetaData) = Json.obj(
      "action" -> article.action,
      "description" -> article.description,
      "_id" -> article._id,
      "posted" -> article.posted,
      "title" -> article.title
    );
  }

  implicit val articleWrites = new Writes[Article]
  {
    def writes(article: Article) = Json.obj(
      "_id" -> article._id.toHexString,
      "content" -> article.content,
      "description" -> article.description,
      "title" -> article.title,
      "url" -> article.url,
      "created" -> article.created,
      "updated" -> article.updated,
      "status" -> article.status
    );
  }


  /*
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action
  {
    implicit request: Request[AnyContent] => Ok(views.html.index())
  }

  def getRecent() = Action.async
  {
    implicit request: Request[AnyContent] =>
    new ArticleRepository(config get[String] "connectionString")
      .getRecent()
      .map(seq => Ok(Json.toJson(seq)))
  }

  /* given the previous ArticleMetaData id, gives the next ArticleMetaData in
   * the sequence. */
  def metadata(nextId:Int) = Action
  {
    implicit request: Request[AnyContent] =>
    val next:ArticleMetaData = null; //new ArticleRepository() getPrevious nextId;
    if (next == null)
    {
      NotFound
    }
    else
    {
      NotFound //Ok(Json.toJson(new ArticleRepository() getPrevious nextId))
    }
  }

  def mostRecentMetadata() = Action.async
  {
    Future { NotFound }
//    implicit request: Request[AnyContent] =>
    //val result:ArticleMetaData = new ArticleRepository() getMostRecentMetadata;
    //val result:Future[ArticleMetaData] = new ArticleRepository(config.get[String]("connectionString")) getMostRecentMetaData;
    //result.map(amd => Ok(Json.toJson(amd)))
//    Ok(Json.toJson(Await.result(result, 100 seconds)));
    /*
    if (result == null)
    {
      NotFound
    }
    else
    {
      Ok(Json.toJson(new ArticleRepository() getMostRecentMetadata));
    }*/
  }


  def preview() = Action
  {
    implicit request:Request[AnyContent] => Ok(views.html.preview());
  }
}
