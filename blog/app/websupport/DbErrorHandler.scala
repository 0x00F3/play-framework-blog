package websupport;

import domain._
import domain.DTO._
import javax.inject._

import play.api._
import play.api.http._
import play.api.mvc._
import play.api.mvc.Results._
import play.api.routing.Router
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class DbErrorHandler @Inject()(
  env: Environment,
  config: Configuration,
  sourceMapper: OptionalSourceMapper,
  router: Provider[Router]
) extends DefaultHttpErrorHandler(env, config, sourceMapper, router)
{
  //*
  override def onProdServerError(request: RequestHeader, exception: UsefulException):
      Future[Result] =
  {
//    new ErrorRepository(connectionString) create ErrorForLog(exception);
    onDevServerError(request, exception)
//    Future { Ok(<h1> hello from the error handler </h1>) }
  }
  //*/

  private[this]  def connectionString: String =
  {
    config.get[String]("connectionString")
  }
}
