package websupport;

import javax.inject.Inject
import play.api.mvc._
import play.api.mvc.Results._
import scala.concurrent._

class AuthenticatedAction @Inject() (parser: BodyParsers.Default)(implicit ec: ExecutionContext) extends ActionBuilderImpl(parser)
{
  override def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]) =
  {
    request.session.get("username") match
    {
      case None => Future.successful(Forbidden("please log in."))
      case Some(u) =>
        {
          if  (u == "admin")
          {
            block(request)
          }
          else { Future.successful(Unauthorized)}
        }
    }
  }
}
