package domain.DTO;

/* collection of case classes for storing data */

import java.util.Date
import org.mongodb.scala.bson._
import play.api._
/* contains the metadata for a blog article. id is necessary to establish 
 * order in case there are two articles from the same day.*/

case class ArticleMetaData
  (
    val action:String, //the url for the blog; should be like the title
    val description:String, //this is how I format code
    val _id:Int, //unique, sequential
    val posted:String, // date in ISO 8601 format. 
    val title:String
  );

case class Article
(
  val _id: ObjectId,
  val content: String,
  val description: String,
  val title: String,
  val url: String, //relative url for article; should be like the title
  val created: Date,
  val updated: Date,
  val status: Boolean
);

case class ErrorForLog
(
  val _id: ObjectId,
  val description: String,
  val stackTrace: String,
  val created: Date,
  val title: String
)

object ErrorForLog
{
  def apply(ex: UsefulException): ErrorForLog =
  {
    ErrorForLog(new ObjectId(), ex.description, ex.cause.getStackTrace().toString(), new Date(), ex.title)
  }

  private[this] def getStackTrace(ex: Option[Throwable]): String = ex match {
    case Some(throwable) => throwable.getStackTrace().toString
    case None => ""
  }
}

/*
case class Session
(
  val _id: ObjectId,
  val username: String,
  val session: String,
  val created: Date,
  val accessed: Date,
  val active: Boolean
); 


object Session
{
  def apply
    (
      username: String,
      session: String,
      created: Date,
      accessed: Date,
      active: Boolean
    ): Session =
    Session(new ObjectId(), username, session, created, accessed, active)
}
 */
