package domain;

/* for reading and writing blog entries
 */
import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings.Builder
import domain.DTO._
import org.bson.codecs.configuration._
import org.bson.codecs.configuration.CodecRegistries._
import org.mongodb.scala._
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.connection.NettyStreamFactoryFactory
import org.mongodb.scala.model._
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Sorts._
import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.language.postfixOps

class ArticleRepository(connectionString:String)
{
  System.setProperty("org.mongodb.async.type", "netty");
  def create(obj: Article): Future[Completed] = 
  {
    getArticleCollection()
      .insertOne(obj)
      .toFuture()
  }

  def getByUrl(url: String): Future[Option[Article]] =
  {
    getArticleCollection()
      .find(Filters.eq("url", url))
      .toFuture()
      .map { seq => if (seq.isEmpty) None else Some(seq.head) }
  }


  def getRecent(): Future[Seq[Article]] =
  {
    getArticleCollection()
      .find()
      .sort(descending("created"))
      .limit(10)
      .toFuture()
  }


  /*----------------------helper methods----------------------------*/

  private[this] def getArticleCollection(): MongoCollection[Article] =
  {
    new MongoCollections(connectionString) getArticleCollection
  }

}
