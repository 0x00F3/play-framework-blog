package domain;

import domain.DTO._
import org.mongodb.scala._
import scala.concurrent._
import scala.language.postfixOps

class ErrorRepository(connectionString: String)
{
  System.setProperty("org.mongodb.async.type", "netty");

  def create(obj: ErrorForLog): Future[Completed] = 
  {
    getErrorCollection
      .insertOne(obj)
      .toFuture()
  }

  private[this] def getErrorCollection: MongoCollection[ErrorForLog] = 
  {
    new MongoCollections(connectionString) getErrorCollection
  }
}
