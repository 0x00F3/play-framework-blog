package domain;
/* encapsulates all collections for MongoDb database */

import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings.Builder
import domain.DTO._
import org.bson.codecs.configuration._
import org.bson.codecs.configuration.CodecRegistries._
import org.mongodb.scala._
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.connection.NettyStreamFactoryFactory

class MongoCollections(connectionString: String)
{
  def getArticleCollection: MongoCollection[Article] =
  {
    getDatabase getCollection "article"
  }

  def getArticleMetaDataCollection: MongoCollection[ArticleMetaData] =
  {
    getDatabase getCollection "article"
  }

  def getErrorCollection: MongoCollection[ErrorForLog] =
  {
    getDatabase getCollection "error"
  }


/*
  def getSessionCollection: MongoCollection[Session] =
  {
    getDatabase getCollection "session"
  }
 */
  /*----------------helper methods-------------------------------*/

  private[this] def getAmdRegistry(): CodecRegistry =
  {
    val amdProvider: CodecRegistry =
      fromProviders(classOf[ArticleMetaData]);
    fromRegistries(amdProvider, DEFAULT_CODEC_REGISTRY)
  }

  private[this] def getDatabase: MongoDatabase =
  {
    val settings: MongoClientSettings = MongoClientSettings.builder()
      .applyConnectionString(new ConnectionString(connectionString))
      .streamFactoryFactory(NettyStreamFactoryFactory())
      .build();
    MongoClient(settings)
      .getDatabase("blogdb")
      .withCodecRegistry(getRegistry)
  }

  private[this] def getRegistry: CodecRegistry =
  {
    val amdProvider: CodecRegistry = fromProviders(classOf[ArticleMetaData]);
    val articleProvider: CodecRegistry = fromProviders(classOf[Article]);
    val errorProvider: CodecRegistry = fromProviders(classOf[ErrorForLog]);
    fromRegistries(DEFAULT_CODEC_REGISTRY, amdProvider, articleProvider)
  }
/*
  private[this] def getSessionRegistry(): CodecRegistry =
  {
    val sessionProvider: CodecRegistry = fromProviders(classOf[Session]);
    fromRegistries(sessionProvider, DEFAULT_CODEC_REGISTRY)
  }
 */
}
